from datetime import datetime
from flask import Flask, render_template
from flask_assets import Environment, Bundle

app = Flask(__name__)

assets = Environment(app)

css = Bundle("scss/*.scss", filters="scss", output="out/bundle.css")
assets.register("gen_css", css)

@app.route("/")
def main():
    cur_time = datetime.now().strftime("%I:%M %P")
    return render_template("index.html", time=cur_time)